import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';

import {NavigationService} from '../../core';

import {AuthService} from '../services';
import {RegistrationInfo} from '../models';

@Component({
  selector: 'wed-register',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.scss']
})
export class RegisterComponent implements OnInit {

  public login: string;
  public password: string;
  public passwordConfirm: string;
  public firstName: string;
  public lastName: string;

  public isProcessing = false;
  public validationMessage: string;

  constructor(private autSvc: AuthService, private navigationSvc: NavigationService) {
  }

  ngOnInit() {
    this.autSvc.authenticatedUserChange.subscribe(
      (credentials) => {
        this.isProcessing = false;
        if (credentials) {
          this.navigationSvc.goToDashboard();
        }
      });
  }

  public doRegister(f: NgForm): boolean {
    console.log("form: " + f.toString());
    console.log("valid: " + f.valid );
    console.log("confirmedPassword " + (f.value.password == f.value.passwordConfirm))
    if (f && f.valid && f.value.password == f.value.passwordConfirm) {
      this.isProcessing = true;
      this.autSvc.register(new RegistrationInfo(
        f.value.login,
        f.value.password,
        f.value.firstName,
        f.value.lastName), () => {
        this.validationMessage = "The specified username already exists";
      });
    } else {
      this.validationMessage = "Input data is not valid";
    }

    return false;
  }
}
