import {Component} from '@angular/core';
import {AuthService} from "./auth/services";
import {Account} from "./auth/models";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  private authUserChange: Subscription;
  private account: Account;

  constructor(private authService: AuthService) {
    this.authUserChange  = this.authService.authenticatedUserChange.subscribe((data:Account) => { this.account = data});
  }

  ngOnInit() {
    this.account = this.authService.authenticatedUser;
  }

  public get isLoggedIn() {
    return this.authService.hasToken();
  }

}
