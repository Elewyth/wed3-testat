import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'wed-all-transactions',
  templateUrl: 'all-transactions.component.html',
  styleUrls: ['transaction-list.component.scss']
})
export class AllTransactionsComponent implements OnInit {
  yearOptions: Array<any>;
  monthOptions: Array<any>;

  public year: number;
  public month: number;
  public count = 10;

  constructor() {
    this.yearOptions = AllTransactionsComponent.getYearOptions();
    this.monthOptions = AllTransactionsComponent.getMonthOptions();
  }

  ngOnInit() {

  }

  static getYearOptions() {
    const years = [];
    const currentYear = new Date().getFullYear();
    for (let i = 0; i < 3; i++) {
      years.push({
        text: currentYear - i,
        value: currentYear - i
      });
    }
    return years;
  }

  static getMonthOptions() {
    const months = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"];

    return months.map((v, i) => {
      return {
        text: v,
        value: i
      };
    });
  }
}
