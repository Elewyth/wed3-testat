import {Component, Input, OnInit} from '@angular/core';
import {MatTableDataSource} from "@angular/material";

import {Transaction} from "../models";
import {AccountService} from "../services";

@Component({
  selector: 'wed-transaction-list',
  templateUrl: 'transaction-list.component.html',
  styleUrls: ['transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit {
  transactions: Array<any>;
  dataSource: MatTableDataSource<Transaction>;
  displayedColumns = ['date', 'from', 'target', 'amount', 'total'];

  private _year: number;
  private _month: number;
  private _count = 10;

  constructor(private accountService: AccountService) {
    this.transactions = [];
  }

  ngOnInit() {
    this.updateTransactions();
  }

  @Input()
  set year(year: number) {
    this._year = year;
    this.updateTransactions();
  }

  @Input()
  set month(month: number) {
    this._month = month;
    this.updateTransactions();
  }

  @Input()
  set count(count: number) {
    this._count = count;
    this.updateTransactions();
  }

  updateTransactions() {
    this.accountService.getTransactions(this.calculateFromDate(), this.calculateToDate(), this._count, undefined).subscribe(data => {
      this.transactions = data;
      this.dataSource = new MatTableDataSource(this.transactions);
    });
  }

  calculateFromDate(): Date {
    if (!this._year) {
      return undefined;
    }

    let month = this._month;
    if (month === undefined) {
      month = 0;
    }

    return new Date(this._year, month);
  }

  calculateToDate(): Date {
    if (!this._year) {
      return undefined;
    }

    if (this._month !== undefined) {
      return new Date(this._year, this._month + 1, 0);
    }

    return new Date(this._year, 11, 31);
  }
}
