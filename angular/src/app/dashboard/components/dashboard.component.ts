import {Component, OnInit, ViewChild} from '@angular/core';
import {AccountService} from "../services";
import {Account} from "../../auth/models";
import {TransactionListComponent} from "./transaction-list.component";

@Component({
  selector: 'wed-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @ViewChild(TransactionListComponent) transactionList: TransactionListComponent;

  accountDetails: { accountNr: string, amount: number, owner: Account };
  targetAccountNr: string;
  targetAccountNrMessage = 'Please specify the target account number';
  targetAccountNrValid = false;
  transferAmount: number;
  transferAmountMessage = 'Please specify the amount';
  transferAmountValid = false;
  showPaymentConfirmation = false;

  constructor(private accountService: AccountService) {
  }

  ngOnInit() {
    this.updateAccountDetails();
  }

  updateAccountDetails() {
    this.accountService.getAccountDetails().subscribe(data => {
      this.accountDetails = data;
    });
  }

  onTargetAccountNrChanged(newValue) {
    this.targetAccountNr = newValue;
    this.targetAccountNrValid = false;
    this.accountService.getAccount(newValue).subscribe(data => {
      if (!newValue) {
        this.targetAccountNrMessage = 'Please specify the target account number';
        return;
      }

      if (data && data.owner) {
        if (newValue === this.accountDetails.accountNr) {
          this.targetAccountNrMessage = 'Cannot transfer to same account';
        } else {
          this.targetAccountNrMessage = `${data.owner.firstname} ${data.owner.lastname}`;
          this.targetAccountNrValid = true;
        }
      }
    }, () => {
      if (newValue) {
        this.targetAccountNrMessage = 'Unknown account number specified';
      } else {
        this.targetAccountNrMessage = 'Please specify the target account number';
      }
    });
  }

  onTransferAmountChanged(newValue) {
    this.transferAmount = newValue;
    this.transferAmountValid = false;
    if (newValue === null) {
      this.transferAmountMessage = 'Please specify the amount';
      return;
    }

    if (newValue < 0.05) {
      this.transferAmountMessage = 'The amount must be at least 0.05';
    } else if (newValue > this.accountDetails.amount) {
      this.transferAmountMessage = 'Not enough funds available';
    } else if ((newValue * 100) % 5 !== 0) {
      this.transferAmountMessage = 'Amount must be a multiple of 0.05';
    } else {
      this.transferAmountValid = true;
      this.transferAmountMessage = '';
    }
  }

  isPayButtonDisabled() {
    return !this.targetAccountNrValid || !this.transferAmountValid;
  }

  onPayClicked() {
    this.accountService.transfer(this.targetAccountNr, this.transferAmount).subscribe(() => {
      this.updateAccountDetails();
      this.transactionList.updateTransactions();
      this.showPaymentConfirmation = true;
    });
  }

  onStartOverClicked() {
    this.showPaymentConfirmation = false;
    this.targetAccountNr = '';
    this.transferAmount = null;
    this.targetAccountNrMessage = 'Please specify the target account number';
    this.targetAccountNrValid = false;
    this.transferAmountMessage = 'Please specify the amount';
    this.transferAmountValid = false;
  }
}
