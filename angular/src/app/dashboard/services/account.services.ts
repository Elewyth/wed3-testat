import {Injectable} from "@angular/core";
import {AccountResourceService} from "../resources";
import {Transaction} from "../models";
import {Observable} from "rxjs/Observable";
import {Account} from "../../auth/models";


@Injectable()
export class AccountService {

  constructor(private resource: AccountResourceService) {

  }

  public getTransactions(
    fromDate?: Date,
    toDate?: Date,
    count?: number,
    skip?: number
  ): Observable<Array<Transaction>> {
    return this.resource.getTransactions(
      fromDate ? fromDate.toString() : '',
      toDate ? toDate.toString() : '',
      count,
      skip);
  }

  public getAccount(
    accountNr: string
  ): Observable<{
    accountNr: string,
    owner: { firstname: string, lastname: string }
  }> {
    return this.resource.getAccount(accountNr);
  }

  public getAccountDetails(): Observable<{ accountNr: string, amount: number, owner: Account }> {
    return this.resource.getAccountDetails();
  }

  transfer(targetAccountNr: string, transferAmount: number): Observable<Transaction> {
    return this.resource.transfer(targetAccountNr, transferAmount);
  }
}
