import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from "./components";
import {AuthGuard} from "../auth/guards/auth.guard";
import {AllTransactionsComponent} from "./components/all-transactions.component";

const appRoutes: Routes = [
  {
    path: 'dashboard',
    component: null,
    children: [
      { path: 'transactions', component: AllTransactionsComponent },
      { path: '', component: DashboardComponent }
    ],
    canActivate: [AuthGuard],
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes) // !forChild() important
  ],
  exports: [
    RouterModule
  ]
})
export class DashboardRoutingModule {}
