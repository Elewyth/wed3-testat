import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

import {SharedModule} from '../shared/shared.module';
import {MaterialModule} from '../shared/material.module';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {AllTransactionsComponent, DashboardComponent, TransactionListComponent} from './components';
import {AccountService} from "./services";
import {AccountResourceService} from "./resources";

import {AuthGuard} from "../auth/guards";

const EXPORTED_DECLARATIONS = [
  // Declarations (Components / Directives) which can be used outside the Module
  DashboardComponent,
  TransactionListComponent,
  AllTransactionsComponent
];
const INTERNAL_DECLARATIONS = [
  ...EXPORTED_DECLARATIONS,
  // Declarations (Components / Directives) which can be used inside the Module
];
const EXPORTS = [
  ...EXPORTED_DECLARATIONS
  // Components/Directives (or even Modules) to export (available for other modules; and forRoot() )
];

@NgModule({
  declarations: INTERNAL_DECLARATIONS,
  imports: [
    CommonModule, FormsModule,
    // Other Modules to import (imports the exported Components/Directives from the other module)
    SharedModule,
    MaterialModule,
    DashboardRoutingModule
  ],
  exports: EXPORTS,
  providers: [
    // DI Providers (hierarchical)
    // (Services, Tokens, Factories, ...) used from/within this Module; add either here or in forRoot();
    //  * Registers these Classes for the current Module; importing Modules will create new instances (for importing level and below)
    AccountResourceService,
    AuthGuard
  ]
})
export class DashboardModule {
  static forRoot(config?: {}): ModuleWithProviders {
    return {
      ngModule: DashboardModule,
      providers: [
        AccountService
      ]
    };
  }

}
