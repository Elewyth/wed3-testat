import {Injectable} from "@angular/core";
import {ResourceBase} from "../../core/resources";
import {HttpClient} from "@angular/common/http";
import {of} from "rxjs/observable/of";
import {catchError, map} from "rxjs/operators";
import {Transaction} from "../models";
import {Observable} from "rxjs/Observable";
import {Account} from "../../auth/models";


@Injectable()
export class AccountResourceService extends ResourceBase {
  constructor(http: HttpClient) {
    super(http);
  }

  public getTransactions(
    fromDate: string = "",
    toDate: string = "",
    count: number = 3,
    skip: number = 0
  ): Observable<Array<Transaction>> {
    return this.get(`/accounts/transactions?fromDate=${fromDate}&toDate=${toDate}&count=${count}&skip=${skip}`)
      .pipe(
        map((data: any) => {
          if (data && data.result instanceof Array) {
            const result = [];
            data.result.forEach(el => result.push(Transaction.fromDto(el)));
            return result;
          }
          return [];
        }),
        catchError((error: any) => of<Array<Transaction>>(null))
      );
  }

  public getAccount(accountNr: string): Observable<{
    accountNr: string,
    owner: { firstname: string, lastname: string }
  }> {
    return this.get(`/accounts/${accountNr}`);
  }

  public getAccountDetails(): Observable<{ accountNr: string, amount: number, owner: Account }> {
    return this.get('/accounts');
  }

  transfer(target: string, amount: number) {
    return this.post('/accounts/transactions', {target, amount});
  }
}
