// @flow

import React from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import {Container} from "semantic-ui-react";

import HeaderMenu from "./components/HeaderMenu";
import Home from "./components/Home";
import Login from "./components/Login";
import Signup from "./components/Signup"
import Dashboard from "./components/Dashboard";
import AllTransactions from "./components/AllTransactions";

import PrivateRoute from "./components/PrivateRoute";

import type {User} from "./api";
import * as api from "./api";

type State = {
  isAuthenticated: boolean,
  token: ?string,
  user: ?User
};

class App extends React.Component<{}, State> {
  constructor(props: any) {
    super(props);
    const token = sessionStorage.getItem("token");
    const user = sessionStorage.getItem("user");
    if (token && user) {
      this.state = {
        isAuthenticated: true,
        token,
        user: JSON.parse(user)
      };
    } else {
      this.state = {
        isAuthenticated: false,
        token: undefined,
        user: undefined
      };
    }
  }

  authenticate = (
    login: string,
    password: string,
    cb: (error: ?Error) => void
  ) => {
    api
      .login(login, password)
      .then(({ token, owner }) => {
        this.setState({ isAuthenticated: true, token, user: owner });
        sessionStorage.setItem("token", token);
        sessionStorage.setItem("user", JSON.stringify(owner));
        cb(null);
      })
      .catch(error => cb(error));
  };

  signout = () => {
    this.setState({
      isAuthenticated: false,
      token: undefined,
      user: undefined
    });
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("user");
  };

  render() {
    const { isAuthenticated, user, token } = this.state;

    return (
      <Router>
        <Container fluid={true}>
          <Route
	          path="/login"
              children={({match}) => (
                  <HeaderMenu
                      isAuthenticated={isAuthenticated}
                      user={user}
                      isLogin={match}
                      signout={event => {
	                      event.preventDefault();
	                      this.signout();
                      }}
                  />
              )}
          />
          <Route
            exact
            path="/"
            render={props => (
              <Home {...props} isAuthenticated={isAuthenticated} />
            )}
          />
          <Route
            path="/login"
            render={props => (
              <Login {...props} authenticate={this.authenticate} />
            )}
          />
          <Route
              path="/signup"
              render={() => (
                  <Signup authenticate={this.authenticate} />
              )}
               />
          {/*
            The following are protected routes that are only available for logged-in users. We also pass the user and token so 
            these components can do API calls. PrivateRoute is not part of react-router but our own implementation.
          */}
          <PrivateRoute
            path="/dashboard"
            isAuthenticated={isAuthenticated}
            token={token}
            user={user}
            component={Dashboard}
          />
          <PrivateRoute
            path="/transactions"
            isAuthenticated={isAuthenticated}
            token={token}
            user={user}
            component={AllTransactions}
          />
        </Container>
      </Router>
    );
  }
}

export default App;
