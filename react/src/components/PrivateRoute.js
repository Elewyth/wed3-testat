import React from "react";
import {Redirect, Route} from "react-router-dom";

function PrivateRoute({component, isAuthenticated, user, token, ...rest}) {
	const render = isAuthenticated ?
		// if the user is authenticated, just render the component
		props => React.createElement(component, {...props, user, token}) :
		// otherwise redirect to the login page
		props => (
			<Redirect
				to={{pathname: "/login", state: {from: props.location}}}
			/>
		);
	return (
		<Route
			{...rest}
			render={render}
		/>
	);
}

export default PrivateRoute;
