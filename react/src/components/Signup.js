// @flow

import React from "react";
import {Redirect} from "react-router-dom";
import {Button, Container, Form, Header, Input, Label, Segment} from 'semantic-ui-react'

import {signup} from "../api";

class Signup extends React.Component<{}, *> {
    state = {
        login: "",
        firstName: "",
        lastName: "",
        password: "",
        confirmPassword: "",
        error: null,
        redirectToReferrer: false,
        loginComment: true,
        passwordComment: true,
        firstNameComment: true,
        lastNameComment: true,
        confirmPasswordComment: true,
        unequalPasswordsComment: false
    };

    handleChange = (event) => {
        let fieldName = event.target.name;
        let fieldCommentName = fieldName + 'Comment';
        let fieldValue = event.target.value;
        let fieldCommentValue;

        if(fieldName === 'firstName' || fieldName === 'lastName'){
            fieldCommentValue = (fieldValue.length < 1);
        } else{
            fieldCommentValue = (fieldValue.length < 3);
        }

        this.setState({[fieldName]: fieldValue, [fieldCommentName]: fieldCommentValue});
    };

    handleSubmit = (event: Event) => {
        event.preventDefault();
        const {login, firstName, lastName, password} = this.state;
        if (this.state.confirmPassword === this.state.password) {
            this.setState({unequalPasswordsComment: false});
            signup(login, firstName, lastName, password)
                .then(result => {
                    console.log("Signup result ", result);
                    this.props.authenticate(login, password, error => {
                        if (error) {
                            this.setState({error: error});
                        } else {
                            this.setState({redirectToReferrer: true, error: null});
                        }
                    });
                })
                .catch(error =>
                    this.setState({error: error})
                );
        } else {
            return this.setState({unequalPasswordsComment: true});
        }

    };

    render() {
        const {redirectToReferrer, error} = this.state;

        if (redirectToReferrer) {
            return <Redirect to="/"/>;
        }

        return (
            <Container fluid className='content-container'>
                <Form>
                    <Header as='h2' attached='top'>
                        Sign Up with a new Account
                    </Header>
                    <Segment attached>
                        <Form.Field>
                            <label>First Name</label>
                            <input
                                onChange={this.handleChange.bind(this)} name='firstName'
                                placeholder="First Name"
                                value={this.state.firstName}
                            />
                            {this.state.firstNameComment &&
                            <Label pointing>Please specify your First Name</Label>}
                        </Form.Field>

                        <Form.Field>
                            <label>Last Name</label>
                            <input
                                onChange={this.handleChange.bind(this)} name='lastName'
                                placeholder="Last Name"
                            />
                            {this.state.lastNameComment &&
                            <Label pointing>Please specify your Last Name</Label>}
                        </Form.Field>

                        <Form.Field>
                            <label>Login</label>
                            <Input onChange={this.handleChange.bind(this)} name='login'
                                   icon='user' iconPosition='left'
                                   placeholder='Username'
                                   value={this.state.login}/>
                            {this.state.loginComment &&
                            <Label pointing>Please specify your login, at least three characters</Label>}
                        </Form.Field>
                        <Form.Field>
                            <label>Password</label>
                            <Input onChange={this.handleChange.bind(this)} name='password'
                                   icon='lock' iconPosition='left'
                                   type='password' placeholder='Password'
                                   value={this.state.password}/>
                            {this.state.passwordComment &&
                            <Label pointing>Please specify your password, at least three characters</Label>}
                        </Form.Field>
                        <Form.Field>
                            <label>Confirm Password</label>
                            <Input onChange={this.handleChange.bind(this)} name='confirmPassword'
                                   icon='lock' iconPosition='left'
                                   type='password' placeholder='Confirm Password'
                                   value={this.state.confirmPassword}/>
                            {this.state.confirmPasswordComment &&
                            <Label pointing>Please confirm your password, at least three characters</Label>
                            }
                            {this.state.unequalPasswordsComment &&
                            <Label pointing>Your Passwords aren't equal! Please confirm your Password</Label>
                            }
                        </Form.Field>
                        <Button type='SignIn' onClick={this.handleSubmit}>Create Account</Button>
                    </Segment>
                </Form>
                {error &&
                <div className="ui negative message">

                    <div className="header">
                        Something went wrong!
                    </div>
                    <p>Please try again</p>
                </div>
                }
            </Container>
        );
    }
}

export default Signup;
