// @flow

import React from 'react';
import ReactDOMServer from 'react-dom/server';
import {Link} from 'react-router-dom';
import {Button, Container, Form, Grid, Header, Input, Segment} from 'semantic-ui-react';
import NumberFormat from 'react-number-format';
import * as api from "../api";
import {TransactionList} from "./AllTransactions";

export type Props = {
	getTransactions: (
		token: string,
		fromDate: string,
		toDate: string,
		count: number,
		skip: number,
		cb: (error: ?Error) => void
	) => void
};

class Dashboard extends React.Component<Props, *> {

	state = {
		login: "",
		password: "",
		error: undefined,
		redirectToReferrer: false,
		accountDetails: undefined,
		targetAccount: "",
		targetAccountOwner: undefined,
		amount: undefined,
		transactions: [],
		showConfirmation: false
	};

	componentDidMount() {
		this.updateAccountDetails(this.errorLog);
		this.updateTransactions(this.errorLog);
	};

	handleAmountChanged = (event: Event) => {
		if (event.target instanceof HTMLInputElement) {
			this.setState({amount: event.target.value});
		}
	};

	handleTargetChanged = (event: Event) => {
		if (event.target instanceof HTMLInputElement) {
			this.setState({targetAccount: event.target.value}, () => {
				api.getAccount(this.state.targetAccount, this.props.token)
					.then(({accountNr, owner}) => {
						this.setState({targetAccountOwner: owner});
					})
					.catch(() => this.setState({targetAccountOwner: undefined}));
			});
		}
	};

	updateAccountDetails = (
		cb: (error: ?Error) => void
	) => {
		api
			.getAccountDetails(
				this.props.token)
			.then(({accountNr, amount}) => {
				this.setState({accountDetails: amount});
				cb(null);
			})
			.catch(error => cb(error));
	};


	handleTransaction = (
		cb: (error: ?Error) => void
	) => {
		api
			.transfer(this.state.targetAccount, this.state.amount, this.props.token)
			.then(() => {
				this.updateAccountDetails((error) => {
					if (error) {
						console.error(error);
					} else {
						this.setState({showConfirmation: true});
					}
				});
				this.updateTransactions(cb);
			})
			.catch(error => cb(error));
	};

	updateTransactions = (
		cb: (error: ?Error) => void
	) => {
		api
			.getTransactions(this.props.token)
			.then(({result}) => {
				this.setState({transactions: result});
				cb(null);
			})
			.catch(error => cb(error));
	};

	errorLog = (err) => {
		if (err) {
			console.error(err);
		}
	};

	startOver = () => {
		this.setState({showConfirmation: false});
	};

	render() {
		const renderFormattedAmountNumber = (value) => (
			<NumberFormat decimalScale={2}
			              fixedDecimalScale={true}
			              thousandSeparator={true}
			              displayType={'text'}
			              renderText={value => value}
			              value={value}/>
		);

		const renderInput = () => {
			return (
				<Segment attached>
					<Form.Field>
						<label>From:</label>
						<div className="ui disabled input">
							<Input
								value={this.props.user.accountNr + " [" +
								ReactDOMServer.renderToString(renderFormattedAmountNumber(this.state.accountDetails)) +
								" CHF]"}
							/>
						</div>
					</Form.Field>
					<Form.Field>
						<label>To:</label>
						<Input onChange={this.handleTargetChanged}
						       placeholder="Target Account Number"
						/>
						{this.state.targetAccount ?
							(this.state.targetAccountOwner ?
								(this.state.targetAccount === this.props.user.accountNr ?
									"Cannot transfer to same account" :
									`${this.state.targetAccountOwner.firstname} ${this.state.targetAccountOwner.lastname}`) :
								"Unknown account number specified") :
							"Please specify the target account number"}
					</Form.Field>
					<Form.Field>
						<label>Amount [CHF]:</label>
						<Input onChange={this.handleAmountChanged}
						       placeholder="Amount in CHF"
						       type="number"
						       step="0.05"
						/>
						{this.state.amount ?
							(this.state.amount > this.state.accountDetails ?
								"Not enough funds available" :
								(this.state.amount <= 0 ?
									"The amount must be at least 0.05" :
									"")) :
							"Please specify the amount"
						}
					</Form.Field>
					<Button primary
					        onClick={() => this.handleTransaction(this.errorLog)}
					        disabled={!(this.state.targetAccountOwner
						        && this.state.targetAccount !== this.props.user.accountNr
						        && this.state.amount > 0
						        && this.state.amount <= this.state.accountDetails)}>Pay</Button>
				</Segment>
			);
		};

		const renderConfirmation = () => {
			return (
				<Segment attached>
					<p>Transaction to {this.state.targetAccount} succeeded!</p>
					<p>New balance {this.state.accountDetails} CHF</p>
					<Button primary onClick={this.startOver}>Start over</Button>
				</Segment>
			);
		};

		return (
			<Container fluid className='content-container'>
				<Grid columns='equal'>
					<Grid.Column width={6}>
						<Form>
							<Header as='h2' attached='top'>
								New Payment
							</Header>
							{this.state.showConfirmation ? renderConfirmation() : renderInput()}
						</Form>
					</Grid.Column>
					<Grid.Column width={10}>
						<Header as='h2' attached='top'>
							Latest Transactions
						</Header>
						<Segment attached>
							<TransactionList
								transactions={this.state.transactions}
							/>
						</Segment>
						<Segment attached>
							<Link to={'/transactions'}>
								<Button primary>All Transactions</Button>
							</Link>
						</Segment>
					</Grid.Column>
				</Grid>
			</Container>
		);
	}
}


export default Dashboard;
