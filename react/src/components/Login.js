// @flow

import React from 'react';
import {Redirect} from 'react-router-dom';
import {Button, Container, Form, Header, Input, Label, Segment} from 'semantic-ui-react'

export type Props = {
    /* Callback to submit an authentication request to the server */
    authenticate: (
        login: string,
        password: string,
        callback: (error: ?Error) => void
    ) => void,
    /* We need to know what page the user tried to access so we can
       redirect after logging in */
    location: {
        state?: {
            from: string
        }
    }
};

export default class Login extends React.Component<Props, *> {
    state = {
        login: "",
        password: "",
        error: undefined,
        redirectToReferrer: false,
        loginComment: true,
        passwordComment: true
    };

    handleChange = (event) => {
        let fieldName = event.target.name;
        let fieldCommentName = fieldName + 'Comment';
        let fieldValue = event.target.value;
        let fieldCommentValue = (fieldValue.length < 3);
        this.setState({[fieldName]: fieldValue, [fieldCommentName]: fieldCommentValue});
    };

    handleSubmit = (event: Event) => {
        event.preventDefault();
        const {login, password} = this.state;
        this.props.authenticate(login, password, error => {
            if (error) {
                this.setState({error});
            } else {
                this.setState({redirectToReferrer: true, error: null});
            }
        });

    };

    render() {
        const {from} = this.props.location.state || {
            from: {pathname: "/dashboard"}
        };
        const {redirectToReferrer, error} = this.state;

        if (redirectToReferrer) {
            return <Redirect to={from}/>;
        }

        return (
            <Container fluid className='content-container'>
                <Form>
                    <Header as='h2' attached='top'>
                        Welcome to the Finance Portal.
                    </Header>
                    <Segment attached>
                        <Form.Field>
                            <label>User name</label>
                            <Input onChange={this.handleChange.bind(this)} name='login'
                                   icon='user' iconPosition='left'
                                   placeholder='Username'/>
                            {this.state.loginComment &&
                            <Label pointing>Please specify your login, at least three characters</Label>}
                        </Form.Field>
                        <Form.Field>
                            <label>Password</label>
                            <Input onChange={this.handleChange.bind(this)} name='password'
                                   icon='lock' iconPosition='left'
                                   type='password' placeholder='Password'/>
                            {this.state.passwordComment &&
                            <Label pointing>Please specify your password, at least three characters</Label>}
                        </Form.Field>
                        <Button type='Log-In' onClick={this.handleSubmit}
                                disabled={(this.state.passwordComment || this.state.loginComment)}>Log-In</Button>
                    </Segment>
                </Form>

                {error &&
                <div className="ui negative message">
                    <div className="header">
                        Es ist ein Fehler aufgetreten!
                    </div>
                    <p>Der Benutzername oder das Kennwort ist falsch</p>
                </div>
                }
            </Container>
        );
    }
}
