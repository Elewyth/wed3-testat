// @flow

import React from 'react'
import {Redirect} from 'react-router-dom'

export type Props = {
	isAuthenticated: boolean,
}

const Home = ({isAuthenticated}: Props) => {
	const pathname = isAuthenticated ? '/dashboard' : '/login';
	return (
		<Redirect
			to={{pathname}}
		/>
	)
};

export default Home
