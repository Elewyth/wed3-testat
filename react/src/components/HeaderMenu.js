// @flow

import React from 'react'
import {Link, NavLink} from 'react-router-dom';
import {Button, Menu} from 'semantic-ui-react'

import type {User} from "../api";

export type Props = {
	isAuthenticated: boolean,
	user: User,
	isLogin: boolean,
	signout: (() => void) => void
};

class HeaderMenu extends React.Component<Props, *> {

	render() {
		if (this.props.isAuthenticated && this.props.user) {
			return (
				<Menu>
					<Menu.Item header>Bank Of Rapperswil</Menu.Item>

					<Menu.Item name='home' as={NavLink} to='/dashboard'>
						Home
					</Menu.Item>

					<Menu.Item name='transaction' as={NavLink} to='/transactions'>
						Transactions
					</Menu.Item>

					<Menu.Menu position='right'>
						<Menu.Item>
							<Link to={'/logout'}>
								<Button primary
								        onClick={this.props.signout}
								>Logout {this.props.user.firstname} {this.props.user.lastname}</Button>
							</Link>
						</Menu.Item>
					</Menu.Menu>
				</Menu>
			);
		} else {
			return (
				<Menu>
					<Menu.Item header>Bank Of Rapperswil</Menu.Item>

					<Menu.Item name='home' as={NavLink} to='/login'>
						Home
					</Menu.Item>

					<Menu.Menu position='right'>
						<Menu.Item>
							<Link to={this.props.isLogin ? '/signup' : '/login'}>
								<Button primary>{this.props.isLogin ? 'Sign Up' : 'Login'}</Button>
							</Link>
						</Menu.Item>
					</Menu.Menu>
				</Menu>
			)
		}
	}
}

export default HeaderMenu;