// @flow

import React from 'react';
import NumberFormat from 'react-number-format'
import Moment from 'react-moment';
import {Container, Dropdown, Form, Header, Input, Segment, Table} from 'semantic-ui-react';

import * as api from "../api";

export function TransactionList({transactions}) {
	const headerRow = ['Date', 'From', 'To', 'Amount [CHF]', 'Balance [CHF]'];

	const renderFormattedAmountNumber = (value) => (
		<NumberFormat decimalScale={2}
		              fixedDecimalScale={true}
		              thousandSeparator={true}
		              displayType={'text'}
		              renderText={value => value}
		              value={value}/>
	);

	const renderAmountCell = (value) => (
		<Table.Cell textAlign={'right'}>
			{renderFormattedAmountNumber(value)}
		</Table.Cell>
	);

	const renderBodyRow = ({date, from, target, amount, total}, i) => ({
		key: `row-${i}`,
		positive: amount > 0,
		negative: amount < 0,
		cells: [
			(<Table.Cell><Moment format="DD.MM.YYYY">{date}</Moment></Table.Cell>),
			from,
			target,
			renderAmountCell(amount),
			renderAmountCell(total)
		]
	});

	return (
		<Table
			celled
			headerRow={headerRow}
			renderBodyRow={renderBodyRow}
			tableData={transactions}
		/>
	);
}

export default class AllTransactions extends React.Component<{}, *> {
	static NUM_PAST_YEARS_TO_SHOW = 3;

	state = {
		filter: {count: 10},
		query: {},
		error: undefined,
		transactions: []
	};

	updateTransactions = (
		cb: (error: ?Error) => void
	) => {
		api
			.getTransactions(
				this.props.token,
				this.calculateFromDate(),
				this.calculateToDate(),
				this.state.filter.count || 10,
				this.state.filter.skip)
			.then(({result, query}) => {
				this.setState({transactions: result, query: query});
				cb(null);
			})
			.catch(error => cb(error));
	};

	errorLog = (err) => {
		if (err) {
			console.error(err);
		}
	};

	componentDidMount() {
		this.updateTransactions(this.errorLog);
	};

	updateYearFilter = (e, {value}) => {
		this.setState({filter: {...this.state.filter, year: value}}, () => this.updateTransactions(this.errorLog));
	};

	updateMonthFilter = (e, {value}) => {
		this.setState({filter: {...this.state.filter, month: value}}, () => this.updateTransactions(this.errorLog));
	};

	updateCountFilter = (e, {value}) => {
		this.setState({filter: {...this.state.filter, count: value || undefined}});
	};

	calculateFromDate() {
		if (!this.state.filter.year) {
			return undefined;
		}

		let month = this.state.filter.month;
		if (month === undefined) {
			month = 0;
		}

		return new Date(this.state.filter.year, month);
	}

	calculateToDate() {
		if (!this.state.filter.year) {
			return undefined;
		}

		if (this.state.filter.month !== undefined) {
			return new Date(this.state.filter.year, this.state.filter.month + 1, 0);
		}

		return new Date(this.state.filter.year, 11, 31);
	}

	static getYearOptions() {
		let result = [{text: '', value: null}];
		for (let i = 0; i < AllTransactions.NUM_PAST_YEARS_TO_SHOW; i++) {
			const year = new Date().getFullYear() - i;
			result.push({
				key: year,
				text: year,
				value: year
			});
		}
		return result;
	};

	static getMonthOptions() {
		let months = ["January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December"];
		let result = months.map((v, i) => {
			return {
				text: v,
				value: i
			};
		});

		result.unshift({
			text: "",
			value: null
		});

		return result;
	};

	render() {
		return (
			<Container fluid className='content-container'>
				<Segment attached="top">
					<Header as="h2">All Transactions</Header>
				</Segment>
				<Segment attached>
					<div>Filter:
						<Form onSubmit={() => this.updateTransactions(this.errorLog)}>
							<Dropdown selection options={AllTransactions.getYearOptions()}
							          onChange={this.updateYearFilter.bind(this)}/>
							<Dropdown selection options={AllTransactions.getMonthOptions()}
							          onChange={this.updateMonthFilter}/>
							<Input placeholder="Limit (Default 10)" onChange={this.updateCountFilter}
							       onBlur={() => this.updateTransactions(this.errorLog)}/>
						</Form>
					</div>
					<div>Showing {this.state.transactions.length} of {this.state.query.resultcount} transactions</div>
					<TransactionList
						transactions={this.state.transactions}
					/>
				</Segment>
			</Container>
		);
	}
}
